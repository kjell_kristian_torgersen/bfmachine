/*
 * BFOptimizingInterpreter.hpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#ifndef BFOPTIMIZINGINTERPRETER_HPP_
#define BFOPTIMIZINGINTERPRETER_HPP_

#include <vector>
#include <IBFMachine.hpp>

class BFOptimizingInterpreter: public IBFMachine {
public:
	BFOptimizingInterpreter(std::vector<char> program, size_t memory_size);
	void Execute(void);
	virtual ~BFOptimizingInterpreter();
private:
	std::vector<char> optimized;
	std::vector<unsigned int> args;
	char * memory;
};

#endif /* BFOPTIMIZINGINTERPRETER_HPP_ */
