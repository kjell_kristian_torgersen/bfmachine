/*
 * util.hpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#ifndef UTIL_HPP_
#define UTIL_HPP_

#include <vector>
#include <iostream>

std::vector<char> File_ReadAllBytes(std::string filename);


#endif /* UTIL_HPP_ */
