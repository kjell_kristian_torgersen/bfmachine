#include <cstdlib>

#ifndef IBFMACHINE_HPP_
#define IBFMACHINE_HPP_

class IBFMachine {
public:
	virtual void Execute(void)=0;
	//IBFMachine();
	virtual ~IBFMachine(){}
};

#endif /* IBFMACHINE_HPP_ */
