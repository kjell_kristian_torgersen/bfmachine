/*
 * BFJitCompiler.hpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#ifndef BFJITCOMPILER_HPP_
#define BFJITCOMPILER_HPP_

#include <IBFMachine.hpp>
#include <vector>

class BFJitCompiler: public IBFMachine {
public:
	BFJitCompiler(std::vector<char> program, size_t memory_size);
	void Execute(void);
	virtual ~BFJitCompiler();
private:
	void(*machinecode)(void*);
	char * memory;
};

#endif /* BFJITCOMPILER_HPP_ */
