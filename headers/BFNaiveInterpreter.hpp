

#ifndef BFINTERPRETER_HPP_
#define BFINTERPRETER_HPP_

#include <iostream>
#include <vector>
#include "IBFMachine.hpp"

class BFNaiveInterpreter : public IBFMachine {
public:
	BFNaiveInterpreter(std::vector<char> program, size_t memory_size);
	void Execute(void);
	virtual ~BFNaiveInterpreter();
private:
	std::vector<char> program;
	char * memory;
};

#endif /* BFINTERPRETER_HPP_ */
