/*
 * BFOptimizingInterpreter.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#include "BFOptimizingInterpreter.hpp"
#include <string.h>
#include <stack>
#include <iostream>
#include <string.h>

BFOptimizingInterpreter::BFOptimizingInterpreter(std::vector<char> program,
		size_t memory_size) :
		memory(new char[memory_size]) {
	std::stack<size_t> stack;
	size_t n = program.size();
	size_t i = 0;
	memset(memory, 0, memory_size);
	program.push_back(0);
	program.push_back(0);
	program.push_back(0);
	while (i < n) {
		switch (program[i]) {
		case '+': {
			size_t start = i;
			while (program[i] == '+')
				i++;
			optimized.push_back(0);
			args.push_back(i - start);
		}
			break;
		case '-': {
			size_t start = i;
			while (program[i] == '-')
				i++;
			optimized.push_back(1);
			args.push_back(i - start);

		}
			break;
		case '>': {
			size_t start = i;
			while (program[i] == '>')
				i++;
			optimized.push_back(2);
			args.push_back(i - start);
		}
			break;
		case '<': {
			size_t start = i;
			while (program[i] == '<')
				i++;
			optimized.push_back(3);
			args.push_back(i - start);
		}
			break;
		case '.':
			optimized.push_back(4);
			args.push_back(1);
			i++;
			break;
		case ',':
			optimized.push_back(5);
			args.push_back(1);
			i++;
			break;
		case '[':
			if (!memcmp(&program[i], "[-]", 3)) {
				optimized.push_back(8);
				args.push_back(1);
				i += 3;
			} else if (!memcmp(&program[i], "[+]", 3)) {
				optimized.push_back(8);
				args.push_back(1);
				i += 3;
			} else {
				stack.push(optimized.size());
				optimized.push_back(6);
				args.push_back(1);
				i++;
			}
			break;
		case ']': {
			size_t ret = stack.top();
			stack.pop();
			optimized.push_back(7);
			args.push_back(ret);
			args[ret] = optimized.size();
			i++;
		}
			break;
		default:
			//putchar(program[i]);
			i++;
			break;
		}
	}
}

void BFOptimizingInterpreter::Execute(void) {
	size_t ip = 0;
	size_t dp = 0;
	size_t n = optimized.size();
	//std::stack<size_t> stack;
	while (ip < n) {
		switch (optimized[ip]) {
		case 0: // +
			memory[dp] += args[ip];
			//for(int u = 0; u < args[ip];u++) std::cout << '+';
			ip++;
			break;
		case 1: // -
			memory[dp] -= args[ip];
			//for(int u = 0; u < args[ip];u++) std::cout << '-';
			ip++;
			break;
		case 2: // >
			dp += args[ip];
			//for(int u = 0; u < args[ip];u++) std::cout << '>';
			ip++;
			break;
		case 3: // <
			dp -= args[ip];
			//for(int u = 0; u < args[ip];u++) std::cout << '<';
			ip++;
			break;
		case 4: // .
			//std::cout << memory[dp];
			putchar(memory[dp]);
			//for(int u = 0; u < args[ip];u++) std::cout << '.';
			ip++;
			break;
		case 5: // ,
			//std::cin >> memory[dp];
			memory[dp] = getchar();
			//for(int u = 0; u < args[ip];u++) std::cout << ',';
			ip++;
			break;
		case 6: { // [
			if (memory[dp] == 0) {
				ip = args[ip];
			} else {
				ip++;
			}
			//for(int u = 0; u < args[ip];u++) std::cout << '[';
			/*if (memory[dp] != 0) {
			 stack.push(ip);
			 ip++;
			 } else {
			 int nesting = 0;
			 ip++;

			 while ((nesting != 0) || ((optimized[ip]) != 7)) {
			 if ((optimized[ip]) == 6) {
			 nesting++;
			 } else if ((optimized[ip]) == 7) {
			 nesting--;
			 }
			 ip++;
			 //std::cerr << optimized[ip];
			 }
			 //std::cerr << std::endl;
			 ip++;
			 }*/
		}
			break;
		case 7: // ]
			ip = args[ip];
			//for(int u = 0; u < args[ip];u++) std::cout << ']';
			//ip = stack.top();
			//stack.pop();
			break;
		case 8: // 0
			//for(int u = 0; u < args[ip];u++) std::cout << '0';
			memory[dp] = 0;
			ip++;
			break;
		default:
			break;
		}
	}
}

BFOptimizingInterpreter::~BFOptimizingInterpreter() {
	delete[] memory;
}

