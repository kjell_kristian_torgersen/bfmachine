/*
 * BFInterpreter.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#include "BFNaiveInterpreter.hpp"
#include "util.hpp"
#include <stack>
#include <vector>
#include <string.h>

BFNaiveInterpreter::BFNaiveInterpreter(std::vector<char> program,
		size_t memory_size) :
		program(program), memory(new char[memory_size]) {
	memset(memory, 0, memory_size);
}

void BFNaiveInterpreter::Execute(void) {
	size_t ip = 0;
	size_t dp = 0;
	std::stack<int> stack;

	while (ip < program.size()) {
		switch (program[ip]) {
		case '+':
			memory[dp]++;
			ip++;
			break;
		case '-':
			memory[dp]--;
			ip++;
			break;
		case '>':
			dp++;
			ip++;
			break;
		case '<':
			dp--;
			ip++;
			break;
		case '.':
			putchar(memory[dp]);
			ip++;
			break;
		case ',':
			memory[dp] = getchar();
			ip++;
			break;
		case '[': {
			if (memory[dp] != 0) {
				stack.push(ip);
				ip++;
			} else {
				int nesting = 0;
				ip++;
				while ((nesting != 0) || (program[ip] != ']')) {
					//Debug.Write((char)code[ip]);
					if (program[ip] == '[') {
						nesting++;
					} else if (program[ip] == ']') {
						nesting--;
					}
					ip++;
				}
				ip++;
			}
		}
			break;
		case ']':
			ip = stack.top();
			stack.pop();
			break;
		default:
			ip++;
			break;
		}
	}
}

BFNaiveInterpreter::~BFNaiveInterpreter() {
	delete[] memory;
}

