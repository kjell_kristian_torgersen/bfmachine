#include <cstdio>
#include <vector>
#include <iostream>

/** \brief Read all bytes specified in *filename and return as array. Use Array_Dispose() on this array when done using it. \warning Don't use on to big files.*/
std::vector<char> File_ReadAllBytes(std::string filename)
{
	std::vector<char> ret;

	// open file
	FILE * f = fopen(filename.c_str(), "rb");
	if (f) {

		// measure file size
		fseek(f, 0, SEEK_END);
		size_t filesize = (size_t) ftell(f);
		// create a perfectly sized array
		ret.resize(filesize);
		fseek(f, 0, SEEK_SET); // go to beginning of file
		fread(ret.data(), 1, filesize, f);
		fclose(f); // close
		return ret;
	}
	return ret;
}
