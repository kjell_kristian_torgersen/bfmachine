//============================================================================
// Name        : bfrun.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//
// Enter (alter /usr/sbin/bfmachine for your preferences)
// 		:brainfuck:E:0:b::/usr/sbin/bfmachine:
// into
// 		/proc/sys/fs/binfmt_misc/register
// to run .b files with this program as they were normal executables on your (linux) system

#include <iostream>

#include "IBFMachine.hpp"
#include "BFNaiveInterpreter.hpp"
#include "BFOptimizingInterpreter.hpp"
#include "BFJitCompiler.hpp"
#include "util.hpp"
#include <string.h>

int main(int argc, char *argv[])
{
	IBFMachine * bf = nullptr;
	std::string filename = "/home/kjell/hello.b";
	int which = 0;
	size_t memory = 65536;
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-naive")) {
			which = 2;
		} else if (!strcmp(argv[i], "-optimizing")) {
			which = 1;
		} else if (!strcmp(argv[i], "-memory")) {
			i++;
			memory = atol(argv[i + 1]);
		} else if (!strcmp(argv[i], "-jit")) {
			which = 0;
		} else if (!strcmp(argv[i], "-h")) {
			std::cout << argv[0] << " [-naive/-optimizing/-jit] [-memory memory_size] brainfuck_file" << std::endl;
			return 0;
			i++;
		} else {
			filename = argv[i];
		}
	}

	std::vector<char> program = File_ReadAllBytes(filename);

	switch (which) {
	case 0:
		bf = new BFJitCompiler(program, memory);
		break;
	case 1:
		bf = new BFOptimizingInterpreter(program, memory);
		break;
	case 2:
		bf = new BFNaiveInterpreter(program, memory);
		break;
	default:
		break;
	}

	bf->Execute();
	delete(bf);
	return 0;
}
