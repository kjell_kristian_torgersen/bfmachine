/*
 * BFJitCompiler.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: kjell
 */

#include "BFJitCompiler.hpp"
#include <cstring>
#include <cstdio>
#include <sys/mman.h>
#include <stack>

BFJitCompiler::BFJitCompiler(std::vector<char> program, size_t memory_size) :
		machinecode(nullptr), memory(new char[memory_size])
{
	std::vector<unsigned char> code;
	std::stack<size_t> stack;
	unsigned char addb[3] = { 0x80, 0x02, 0 };
	unsigned char subb[3] = { 0x80, 0x2A, 0 };

	unsigned char addp[7] = { 0x48, 0x81, 0xC2, 0, 0, 0, 0 };
	unsigned char subp[7] = { 0x48, 0x81, 0xEA, 0, 0, 0, 0 };

	unsigned char putbyte[23] = "\xb8\x01\x00\x00\x00\xbf\x01\x00\x00\x00\x48\x89\xd6\x52\xba\x01\x00\x00\x00\x0f\x05\x5a";
	unsigned char getbyte[19] = "\x48\x31\xC0\x48\x31\xFF\x48\x89\xD6\x52\xBA\x01\x00\x00\x00\x0F\x05\x5A";
	unsigned char cmp[3] = { 0x80, 0x3A, 0x00 };
	unsigned char jmp[5] = { 0xE9, 0, 0, 0, 0 };
	unsigned char je[6] = { 0x0F, 0x84, 0, 0, 0, 0 };

	//unsigned char mov[3] =   { 0xC6, 0x02, 0x00 };
	unsigned char mov[3] = { 0xC6, 0x02, 0x00 };
	//unsigned char callpuchar[17] = { 0x52, 0x48, 0x8b, 0x3a, 0x48, 0xb8, 0, 0, 0, 0, 0, 0, 0, 0, 0xff, 0xD0, 0x5a };

	unsigned char prolog[4] = { 0x55, 0x48, 0x89, 0xFA };
	unsigned char epilog[2] = { 0x5D, 0xC3 };

	code.insert(code.end(), prolog, prolog + sizeof(prolog));
	size_t i = 0;
	while (i < program.size()) {
		switch (program[i]) {
		case '+': // +
		{
			size_t start = i;
			while (program[i] == '+')
				i++;
			addb[2] = (unsigned char) (i - start);
			code.insert(code.end(), addb, addb + sizeof(addb));
		}
			break;
		case '-': // -
		{
			size_t start = i;
			while (program[i] == '-')
				i++;
			subb[2] = (unsigned char) (i - start);
			code.insert(code.end(), subb, subb + sizeof(subb));
		}
			break;
		case '>': // >
		{
			size_t n;
			size_t start = i;
			while (program[i] == '>')
				i++;
			n = i - start;
			memcpy(addp + 3, &n, 4);
			code.insert(code.end(), addp, addp + sizeof(addp));
		}
			break;
		case '<': // <
		{
			size_t n;
			size_t start = i;
			while (program[i] == '<')
				i++;
			n = i - start;
			memcpy(subp + 3, &n, 4);
			code.insert(code.end(), subp, subp + sizeof(subp));
		}
			break;
		case '.': // .
			code.insert(code.end(), putbyte, putbyte + sizeof(putbyte) - 1);
			i++;
			break;
		case ',': // ,
			code.insert(code.end(), getbyte, getbyte + sizeof(getbyte) - 1);
			i++;
			break;
		case '[': // [
			if (!memcmp(&program[i], "[-]", 3)) {
				code.insert(code.end(), mov, mov + sizeof(mov));
				i += 3;
			} else if (!memcmp(&program[i], "[+]", 3)) {
				code.insert(code.end(), mov, mov + sizeof(mov));
				i += 3;
			} else {
				stack.push(code.size());
				code.insert(code.end(), cmp, cmp + sizeof(cmp));
				code.insert(code.end(), je, je + sizeof(je));
				i++;
			}
			break;
		case ']': // ]
		{
			size_t ret = stack.top();
			stack.pop();
			int back = (int) (ret - (int) code.size() - 5);
			int forward = (int) ((int) code.size() - ret - 9 + 5);
			memcpy(jmp + 1, &back, 4);
			memcpy(&code[ret] + 5, &forward, 4);
			code.insert(code.end(), jmp, jmp + sizeof(jmp));
			i++;
		}
			break;
		default:
			//putchar(program[i]);
			i++;
			break;
		}
	}
	code.insert(code.end(), epilog, epilog + sizeof(epilog));

	memset(memory, 0, memory_size);
	machinecode = (void (*)(void*))mmap(0, code.size(), PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	memcpy((void*)machinecode, code.data(), code.size());
}

void BFJitCompiler::Execute(void)
{
	machinecode(memory);
	//munmap((void*)machinecode, 1);
}

BFJitCompiler::~BFJitCompiler()
{
	if (memory) {
		delete[] memory;
		memory = nullptr;
	}
}

